import 'package:flutter/material.dart';
import 'package:flutter_train/UI/list_page.dart';
import 'package:oauth2/oauth2.dart' as oauth2;
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  String _email;
  String _password;
  final formKey = new GlobalKey<FormState>();

  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  void _login() async {
    final authorizationEndpoint =
    Uri.parse("http://jobee-dev-server.sugadev.top:9080/oauth/token");

    // Something like this
    final identifier = "2";
    final secret = "gQxY5OnQ8AgvtXMe1HlvKLmssw007tTSHVYMsAlG";

    // Make a request to the authorization endpoint that will produce the fully
    // authenticated Client.
    try {
      var client = await oauth2.resourceOwnerPasswordGrant(
          authorizationEndpoint, _email, _password,
          identifier: identifier, secret: secret);

      if(client.credentials.accessToken.isNotEmpty) {
        Navigator.push(context, new MaterialPageRoute(
            builder: (context) => AnimatedListSample())
        );
      }
    } catch(e) {
      Fluttertoast.showToast(
          msg: "Unauth",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.black12,
          textColor: Colors.white
      );
    }
  }

  void validateAndLogin() {
    final form = formKey.currentState;
    if(form.validate()) {
      form.save();
      _login();
    }
  }
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/logo.png'),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
//      initialValue: 'alucard@gmail.com',
        textInputAction: TextInputAction.next,
        onSaved: (value) => _email = value,
        focusNode: _emailFocus,
        onFieldSubmitted: (wtf) {
          _emailFocus.unfocus();
          FocusScope.of(context).requestFocus(_passwordFocus);
        },
        validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
        decoration: InputDecoration(
        prefixIcon: Icon(Icons.account_circle),
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final sizeBox = SizedBox(height: 20,);

    final password = TextFormField(
      autofocus: false,
      obscureText: true,
      focusNode: _passwordFocus,
      onSaved: (value) => _password = value,
      onFieldSubmitted: (tem){
      },
      validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
      decoration: InputDecoration(
        fillColor: Color(0xFFfff),
        prefixIcon: Icon(Icons.lock),
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: ButtonTheme(
        minWidth: 250.0,
        height: 50.0,
        child: RaisedButton (
          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
          onPressed: () {
            validateAndLogin();
          },
          color: Color(0xFFB8DB86),
          child: Text('Log In', style: TextStyle(color: Colors.black)),
        ),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );

    return Scaffold(
      body: Container(
        height: double.infinity,
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors: [const Color(0xFF97B80D), const Color(0xFF75B80D)],
              begin: FractionalOffset.topLeft,
              end: FractionalOffset.bottomRight,
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            new Container(
              margin: const EdgeInsets.only(top: 120.0),
              child: logo,
            ),
            new Container(
              margin: const EdgeInsets.only(top: 50.0),
              child: Form(
                key: formKey,
                child: new Column(
                  children: <Widget>[email, sizeBox, password],
                  mainAxisSize: MainAxisSize.max,
                )
              ),
            ),
            SizedBox(height: 24.0),
            loginButton,
            forgotLabel
          ],
        ),
      ),
    );
  }
}

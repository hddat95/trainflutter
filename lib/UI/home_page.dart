import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomePage extends StatefulWidget {
  static String tag = 'home-page';

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final item = Column(
      children: <Widget>[
        new Flexible(
          child: Row(
            children: [
              Flexible(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.access_time,
                      size: 50.0,
                    ),
                    Container(
                      height: 10,
                    ),
                    Text("abc"),
                  ],
                ),
                flex: 1,
              ),
              VerticalDivider(color: Colors.black),
            ],
          ),
          flex: 1,
        ),
        Divider(color: Colors.black)
      ],
  );

  final item2 = Container(
      child: Column(
    mainAxisAlignment: MainAxisAlignment.end,
    children: <Widget>[
      new Flexible(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.access_time,
                    size: 50.0,
                  ),
                  Container(
                    height: 10,
                  ),
                  Text("abc"),
                ],
              ),
              flex: 1,
            ),
          ],
        ),
        flex: 1,
      ),
      Divider(color: Colors.black)
    ],
  ));


  @override
  Widget build(BuildContext context) {
    const PrimaryColor = const Color(0xFF659026);
    return new MaterialApp(
      home: new Scaffold(
          appBar: AppBar(
            backgroundColor: PrimaryColor,
            title: new Image.asset(
              'assets/ic_logo_horizontal.png',
              fit: BoxFit.cover,
              scale: 5,
            ),
            centerTitle: true,
          ),
          body: GridView.count(
            primary: false,
            crossAxisCount: 2,
            children: <Widget>[InkWell(onTap: (){
              Fluttertoast.showToast(
                  msg: "So 1 ",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.black12,
                  textColor: Colors.white
              );
            }, child: item,), item2, item, item2, item],
          )),
    );
  }
}

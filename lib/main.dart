import 'package:flutter/material.dart';
import 'package:flutter_train/oauth/login.dart';

void main() {
  runApp(SampleApp());
}

class SampleApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Learn English',
      home: LoginPage(),
    );
  }
}
